The aim of this project is to understand Bayesian Linear Regression (BLR) algorithm by applying it to a student performance dataset to predict 
the final grades of the students.

The python solution is attached as CW_SDA.py file. It is a class containing multiple functions with unique tasks. The program reads the attached
dataset Merged.csv first, then performs exploratory data analysis, data normalization, features' correlations and finding the 'n' most correlated
features out of 33 features. Implementation of 5 different statistical model is performed which is laters compared with the performance
of BLR using regression ealaution metrics.

The solution is documented in the attached report Student_Performance_Prediction.pdf explaining the solution and displayig the results.
The code is attached as an Appendix. 

The code uses normal Python Libraries like seaborn, matplotlib, pandas, numpy and sklearn. PyMC3 is used in addition to implement BLR.
To run this code, the system should have these packages installed. Once done, run the python code either by command line or from any IDE 
like PyCharm placing the dataset in the same path as the code file.
